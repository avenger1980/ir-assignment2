from geometry_msgs.msg import Pose, PoseArray, Quaternion
from pf_base import PFLocaliserBase
import math
import rospy

from util import rotateQuaternion, getHeading
from random import random

from time import time

import numpy.ma as ma

import numpy as np
import random

import time

import copy

class PFLocaliser(PFLocaliserBase):
       
    def __init__(self):
        # ----- Call the superclass constructor
        super(PFLocaliser, self).__init__()
        
        # ----- Set motion model parameters
        self.ODOM_ROTATION_NOISE = 0.02       # Odometry model rotation noise
        self.ODOM_TRANSLATION_NOISE = 0.02      # Odometry x axis (forward) noise
        self.ODOM_DRIFT_NOISE = 0.2          # Odometry y axis (side-side) noise
        # ----- Sensor model parameters
        self.NUMBER_PREDICTED_READINGS = 20     # Number of readings to predict

        self.NUMBERR_PARTICLE = 1000             #particles number
        self.NUMBERR_PARTICLE_ORIGINAL = 1000    #particles number

        self.genNewParticle_Flat = 0

        self.timeStart = 0
        self.timeEnd = 0

        self.map_xy = list()
        
       
    def initialise_particle_cloud(self, initialpose):
        """
        Set particle cloud to initialpose plus noise

        Called whenever an initialpose message is received (to change the
        starting location of the robot), or a new occupancy_map is received.
        self.particlecloud can be initialised here. Initial pose of the robot
        is also set here.
        
        :Args:
            | initialpose: the initial pose estimate
        :Return:
            | (geometry_msgs.msg.PoseArray) poses of the particles
        """

        #find map's coordinate
        self.map_xy = find_map_coords(self.occupancy_map)
        
        #mark the start time
        self.timeStart = time.time()

        self.sortWeight = list()

        #initialise the particles
        PArray = PoseArray()
        PArray.poses = GenerateParticle(self.NUMBERR_PARTICLE,self.map_xy)
        return PArray
 
    def update_particle_cloud(self, scan):
        """
        This should use the supplied laser scan to update the current
        particle cloud. i.e. self.particlecloud should be updated.
        
        :Args:
            | scan (sensor_msgs.msg.LaserScan): laser scan to use for update

         """
        scan.ranges = ma.masked_invalid(scan.ranges).filled(scan.range_max)
        
        #initialise the variable
        weightList = list()
        newPoseList = list()
        l_weight = list()
        largestNum = 0
        
        #set the generate new particle flat in 0
        self.genNewParticle_Flat = 0 

        #timing(erase and generate particles every 3 secs)
        if self.timeEnd - self.timeStart >= 5:
            self.genNewParticle_Flat = 1 
            self.timeStart = time.time()
            #print("timeStart:" + str(self.timeStart))

        #update the time
        self.timeEnd = time.time()
        #print("timeEnd:" + str(self.timeEnd))

     
        #------------------------------------------------------------------------
        #use RouletteWheel algorithm to filter the particles
        #------------------------------------------------------------------------

        weight_l = list()
        weight = [0,0]
        for i in range(self.NUMBERR_PARTICLE):
            weight = [pow(self.sensor_model.get_weight(scan,self.particlecloud.poses[i]),2),i] 
            weight_l.append(weight)
            largestNum += weight[0]
            l_weight.append(largestNum)
        self.sortWeight = sorted(weight_l)
        
        #erase particle randomly
        last_num = 0
        if self.genNewParticle_Flat == 1:
            for i in range(int(self.NUMBERR_PARTICLE*0.4),int(self.NUMBERR_PARTICLE*0.7)):
                del_num = self.sortWeight[i][1]
                if del_num > last_num:
                    del_num -= i
                del self.particlecloud.poses[del_num]
                last_num = del_num


            self.NUMBERR_PARTICLE = len(self.particlecloud.poses)

        while True:
            luck_num = np.random.uniform(0,largestNum)
            for i in range(self.NUMBERR_PARTICLE):
                source_pose = self.particlecloud.poses[i]
                if luck_num < l_weight[i]:
                    newPoseList.append(UpdateParticle_noise(source_pose,\
                                                    self.ODOM_TRANSLATION_NOISE,\
                                                    self.ODOM_DRIFT_NOISE)) 
                    break
            if len(newPoseList) == self.NUMBERR_PARTICLE:
                break
        
        '''
        -------------------------------------------------------------------------------
        r = np.random.uniform(0,1/self.NUMBERR_PARTICLE)
        c = weight_l[0]
        i = 1
        for m in range(1,self.NUMBERR_PARTICLE+1):
            U = r + (m - 1) / self.NUMBERR_PARTICLE
            while U >= c:
                i += 1
                c += weight_l[i]
            source_pose = self.particlecloud.poses[i]
            newPoseList.append(UpdateParticle_noise(source_pose,\
                                                    self.ODOM_TRANSLATION_NOISE,\
                                                    self.ODOM_DRIFT_NOISE))
        -------------------------------------------------------------------------------
        '''

        #------------------------------------------------------------------------

        #generate new particle to avoid particles degenerate
        if self.genNewParticle_Flat == 1:
            for i in range(self.NUMBERR_PARTICLE_ORIGINAL - self.NUMBERR_PARTICLE):
                newPoseList.append(GenerateParticle(0,self.map_xy))
            self.NUMBERR_PARTICLE = self.NUMBERR_PARTICLE_ORIGINAL 

        #update particle cloud
        self.particlecloud.poses = newPoseList
        
    def estimate_pose(self):
        """
        This should calculate and return an updated robot pose estimate based
        on the particle cloud (self.particlecloud).
        
        Create new estimated pose, given particle cloud
        E.g. just average the location and orientation values of each of
        the particles and return this.
        
        Better approximations could be made by doing some simple clustering,
        e.g. taking the average location of half the particles after 
        throwing away any which are outliers

        :Return:
            | (geometry_msgs.msg.Pose) robot's estimated pose.
         """

        #------------------------------------------------------------------
        #estimate the location by the average of the position and orientation 
        #------------------------------------------------------------------ 
        x = 0
        y = 0
        z = 0
        w = 0

        count = self.NUMBERR_PARTICLE

        for particle in self.particlecloud.poses:
            x += particle.position.x
            y += particle.position.y
            z += particle.orientation.z
            w += particle.orientation.w
            count += 1
        
        average_x = x/count
        average_y = y/count
        average_z = z/count
        average_w = w/count

        pose = Pose()
        pose.position.x = average_x
        pose.position.y = average_y
        pose.orientation.z = average_z
        pose.orientation.w = average_w

        return pose 
        #------------------------------------------------------------------

#------------------------------------------------------------------
# generate particles randomly in the coordinate of the map
#------------------------------------------------------------------
def GenerateParticle(num, map_xy):
    if num is not 0:
        poses_list = list()

        for i in range(num):
            poses_list.append(Pose())

            j = random.randint(0, len(map_xy)-1)
            poses_list[i].position.x = map_xy[j][0]
            poses_list[i].position.y = map_xy[j][1]
            poses_list[i].orientation = rotateQuaternion(Quaternion(w = 1),math.radians(random.uniform(0, 360)))
        return poses_list
    else:
        newpose = Pose()
        j = random.randint(0, len(map_xy)-1)
        newpose.position.x = map_xy[j][0]
        newpose.position.y = map_xy[j][1]
        newpose.orientation.w = 1
        newpose.orientation = rotateQuaternion(newpose.orientation,math.radians(random.uniform(0, 360)))
        return newpose

#------------------------------------------------------------------
# add noise to the particles
#------------------------------------------------------------------
def UpdateParticle_noise(original_pose,x_noise,y_noise):
    newPose = Pose()
    newPose.position.x = random.gauss(original_pose.position.x, x_noise)
    newPose.position.y = random.gauss(original_pose.position.y, y_noise)
    heading = getHeading(original_pose.orientation)
    original_pose.orientation.w = 1
    newPose.orientation = rotateQuaternion(original_pose.orientation,random.vonmisesvariate(heading, 35)) 
    return newPose


#------------------------------------------------------------------
# find map coordinate
#------------------------------------------------------------------
def find_map_coords(occupancy_map):
    """
    Finds the coordinates of all points that are inside the actual map area

    :param occupancy_map: (nav_msgs.OccupancyGrid) world map information
    :return: coord_list: (Array of float tuples) set of pairs of coordinates
    """
    map_data = occupancy_map.data
    size = occupancy_map.info.width
    origin = occupancy_map.info.origin
    resolution = occupancy_map.info.resolution

    # Find indices of all points inside the actual map
    found_indices = [i for i, map_data in enumerate(map_data) if map_data is not -1]
    # Create an array containing them
    found_indices = np.array(found_indices)
    # Reduce the values to coordinates inside a size x size matrix
    actual_indices = found_indices % size
    # Compute two arrays containing X-axis and Y-axis indices for each point inside the map
    x_coords = ((actual_indices + 1) * resolution) + origin.position.x
    y_coords = ((found_indices - actual_indices) + size) / size * resolution + origin.position.y

    # Create a list of tuples (x, y)
    coord_list = zip(x_coords, y_coords)
    return coord_list
